#+REVEAL_HLEVEL: 1
#+REVEAL_EXTRA_CSS: ./custom.css
#+OPTIONS: num:nil toc:nil
#+TITLE: What's new with LXC3?
#+EMAIL: peb@debian.org
#+AUTHOR: Pierre-Elliott Bécue

* LXC??
** Are you from a cave?

Linux Containers

 - Pid and mount namespace
 - Host-run kernel
 - Not *fully* isolated (no Windows container on Debian), but a good compromise without the performance
   hit.

* LXC3 in the archive?
** Rationale

 - LXC3 was out since 2018/03/27.
 - It is LTS and bringing a lot of new features
 - The buster freeze was coming in 2019/01
 - Debian was about to miss the train.

** Major changes

 - No need for the "-n" before the container's name anymore (lazy sysadmin is lazy)
 - Full cgroup2 support
 - Splitting out templates and LUA/Python3 bindings
 - The pam-cgfs module, creating cgroups a user can administer upon
   login got from LXCFS to LXC, as LXCFS is now dedicated to proc masking

** Major changes (2)   

 - Dropping of obsolete config statements (lxc-update-config is your friend)
 - Drop of CGManager (allowing unprivileged containers to have cgroup
   management, and cgroup control for nested containers)
 - Drop of the cgfs driver (old driver to find where were mounted the
   different cgroups file systems), replaced by cgfsng.
 - And a massive cleanup

** Impacts on packaging

 - All the Debian patches had been included
 - AppArmor support was not working
 - Three source packages to add to the archive

$\Rightarrow$ patching needed (thanks for those who helped, some patches are in 3.0.4 upstream now!)

** Why I dived in

By order of importance (*sarcasm?*)

 - I was moving from my dedicated server to a new one
 - Need for a proper architecture
 - Sysadmin, I don't like '-n'
 - Freshly arrived as a DD

* Special topics

** Unprivileged containers

Also known as user containers

 - Rely on user namespaces
 - No superuser involved, except with the lxc-net utilitary to bind a network card

[[./imgs/user_cont.png]]

** Unprivileged containers (2)

 - LXC developers recommend to use unprivileged containers first on a
   security aspect (LSS 2019: https://lssna19.sched.com/event/RHa5)
 - AppArmor integration is moot, use the unconfined profile
 - Some tricky evasions with privilege escalations were possible at
   some point (eg https://lwn.net/Articles/671641/)

** AppArmor integration for privileged containers

 - With systemd v240 or higher, the systemd's confinement options are
   conflicting with the current AppArmor support in LXC 3.0.3.
 - We patched using snippets of code from 3.1 providing the required
   support.
 - Use lxc.apparmor.profile = generated in your configuration
 - Also support of nested containers using lxc.apparmor.allow_nesting.
 - (as previously said) The whole fails in unprivileged containers.
   
* What's missing (and we'd like to see in buster)

** LXD

 - It's a Go daemon which provides a full system container manager
 - Through a REST API (works remotely, yay)
 - Features advanced resources control (CPU, RAM, network...) and live
   container migration in a cluster of hosts.
 - See https://linuxcontainers.org/lxd/introduction/
   
** Missing LXD is missing

 - Go package with (currently) unpackageable dependencies
 - dqlite relies on a Canonical patched version of SQLite
 - Patched version that conflicts with the classic SQLite
 - Canonical's idea is to have the patches merged in SQLite…
   
** Distrobuilder

 - A replacement for lxc-templates
 - Also in Go
 - Allows to build images for both LXC and LXD
 - No unpackageable dependency but a lot of things missing from the
   archive.
